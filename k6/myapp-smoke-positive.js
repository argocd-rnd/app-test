
import http from "k6/http";
import { describe, expect } from 'https://jslib.k6.io/k6chaijs/4.3.4.3/index.js';

export const options = {
    iterations: 1,
};

export default function () {
    const payload = JSON.stringify([ "мск сухонска 11/-89" ]);
    const response = http.post("http://myapp.myapp:8888/address", payload);
    expect(response.status, 'response status').to.equal(200);
    expect(response).to.have.validJsonBody();
}
